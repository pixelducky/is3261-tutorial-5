package com.example.tysonquek.tysoninternet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by tysonquek on 22/9/15.
 */
public class NetworkUtils {
    public static boolean checkNetwork(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
    public static String getJSON(String urlString) {
        HttpURLConnection httpUrlConnection = null;
        URL url = null;
        int responseCode = -1;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            url = new URL(urlString);
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.connect();
            responseCode = httpUrlConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpUrlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String data;
                while ((data = reader.readLine()) != null) {
                    stringBuilder.append(data);
                }
                inputStream.close();
            }

        } catch (Exception e) {
            Log.d(NetworkUtils.class.getSimpleName(), e.getMessage());
            e.printStackTrace();
        } finally {
            httpUrlConnection.disconnect();
        }
        return stringBuilder.toString();
    }
}
