package com.example.tysonquek.tysoninternet.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    static {
        // Add 3 sample items.
        addItem(new DummyItem("Sports","Soccer"));
        addItem(new DummyItem("Food", "French Fries" ));
        addItem(new DummyItem("Transport", "Train"));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.classification, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public String classification;
        public String content;

        public DummyItem(String classification, String content) {
            this.classification = classification;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
