package com.example.tysonquek.tysoninternet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

public class ActivityFour extends AppCompatActivity {

    private EditText mFromCurrency;
    private EditText mToCurrency;
    private TextView mRatesResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_four);
        mFromCurrency = (EditText) findViewById(R.id.et_from_currency);
        mToCurrency = (EditText) findViewById(R.id.et_to_currency);
        mRatesResult = (TextView) findViewById(R.id.tv_rates_result);

        findViewById(R.id.btn_get_rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.checkNetwork(v.getContext())) {
                    if (mToCurrency.getText().toString().isEmpty() || mFromCurrency.getText().toString().isEmpty())
                        return;
                    new MyAsyncTaskFour().execute("http://api.fixer.io/latest?base=" + mFromCurrency.getText().toString());
                } else {
                    Toast.makeText(v.getContext(), "Internet connection is needed for task to run. Please turn on 3G/Wifi.", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_four, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class MyAsyncTaskFour extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            return NetworkUtils.getJSON(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String rate = getRates(s);
            if (rate != null)
                mRatesResult.setText("1 " + mFromCurrency.getText().toString().toUpperCase() + " = " + getRates(s) + " " + mToCurrency.getText().toString().toUpperCase());

        }

        private String getRates(String jsonStr) {
            JSONArray response = new JSONArray();
            String rate = null;
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                JSONObject ratesObj = jsonObject.getJSONObject("rates");
                rate = ratesObj.getString(mToCurrency.getText().toString().toUpperCase());
            } catch (Exception e) {
                Log.d(getClass().getSimpleName(), e.getLocalizedMessage());
            }
            return rate;
        }


    }
}
