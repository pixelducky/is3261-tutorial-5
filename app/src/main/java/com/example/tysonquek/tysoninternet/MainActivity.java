package com.example.tysonquek.tysoninternet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnActivityOne = (Button)findViewById(R.id.btn_activity_one);
        Button btnActivityTwo = (Button)findViewById(R.id.btn_activity_two);
        Button btnActivityThree = (Button)findViewById(R.id.btn_activity_three);
        Button btnActivityFour = (Button)findViewById(R.id.btn_activity_four);

        btnActivityOne.setOnClickListener(this);
        btnActivityTwo.setOnClickListener(this);
        btnActivityThree.setOnClickListener(this);
        btnActivityFour.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_activity_one:
                startActivity(new Intent(this, ActivityOne.class));
                break;
            case R.id.btn_activity_two:
                startActivity(new Intent(this, ActivityTwo.class));
                break;
            case R.id.btn_activity_three:
                startActivity(new Intent(this, ActivityThree.class));
                break;
            case R.id.btn_activity_four:
                startActivity(new Intent(this, ActivityFour.class));
                break;
        }
    }
}
