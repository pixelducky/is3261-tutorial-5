package com.example.tysonquek.tysoninternet;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class ActivityOne extends AppCompatActivity implements ActivityOneFragment.OnFragmentInteractionListener, ActivityOneListFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container_one, new ActivityOneListFragment(), ActivityOneListFragment.class.getSimpleName())
                .add(R.id.fragment_container_two, new ActivityOneFragment(), ActivityOneFragment.class.getSimpleName())
                .commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String classification) {
//        Toast.makeText(this, classification, Toast.LENGTH_SHORT).show();
        ((ActivityOneFragment)getSupportFragmentManager().findFragmentByTag(ActivityOneFragment.class.getSimpleName())).updateItemDetails(classification);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
